const dodosData = [
    {
        id:0,
        text: "Сверстать шапку для сайта",
        complited: true
    },
    {
        id:1,
        text: "Создать форму для отправки сообщения",
        complited: false
    },
    {
        id:2,
        text: "Посмотреть новый урок по JS",
        complited: true
    },
    {
        id:3,
        text: "Погулять",
        complited: true
    },
    {
        id:4,
        text: "Активировать 3Ds max",
        complited: false
    },
]
export default dodosData;