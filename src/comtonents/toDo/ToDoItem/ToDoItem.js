import React from "react";
import "./ToDoItem.scss"

const ToDoItem = ({id, description, complited, handleChange, deleteTask}) => {
    const green = {
        color : "green"
    };
    const red = {
        color : "red"
    };

    

    return (
        <div className = "todoBlock todoBlock_big">
            <input type = "checkbox" 
                onChange = {handleChange} 
                defaultChecked = {complited}
            />
            <div className = "desctiption-wrapper">
                <p className = "description" style = {complited == true ? green : red}>{description}</p>
            </div>
            <button value = {id} onClick = {deleteTask}>delete</button>
             
        </div>
    )
}


export default ToDoItem;