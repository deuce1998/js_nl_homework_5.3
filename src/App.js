import React, {useState} from 'react';
import './App.css';
import ToDoItem from './comtonents/toDo/ToDoItem/ToDoItem';
import todosData from './comtonents/toDo/todosData';

function App() {
  let localStorageTasks;
  localStorage.hasOwnProperty('tasks') && localStorage.getItem('tasks').length > 0 ? localStorageTasks = JSON.parse(localStorage.getItem('tasks')) 
                                                                                          : localStorageTasks = todosData;

  const [todoItems, setTodoItems] = useState(localStorageTasks);
  const [inputValue, setInputValue] = useState('');

  const handleInput = (e) => {
    setInputValue(e.target.value);
  }

  const handleChange = id => {
    const index = todoItems.map(item => item.id).indexOf(id);  
    todoItems[index].complited = !todoItems[index].complited;
    localStorage.setItem('tasks', JSON.stringify([...todoItems]));
    setTodoItems([...todoItems]);    
  }

  const addNewTask = () => {
    let indexArray = todoItems.map(item => item.id);
    const newTask = {
      id: Math.max(...indexArray) + 1,
      text: inputValue,
      complited: false 
    };
    const newTasks = [...todoItems, newTask];
    localStorage.setItem('tasks', JSON.stringify(newTasks));
    setTodoItems([...newTasks]);
  }
  const deleteTask = (deleteIndex) => {
    let newTodoItems = [];
    todoItems.forEach(element => {
      element.id != deleteIndex && newTodoItems.push(element);
    });
    localStorage.setItem('tasks', JSON.stringify([...newTodoItems]));
    setTodoItems([...newTodoItems]);
  }


  const activeTasks = [...todoItems].filter(task => task.complited === false);
  const complitedTasks = [...todoItems].filter(task => task.complited === true);
  const finalTasks = [...activeTasks, ...complitedTasks].map(item => {
    return (
      <ToDoItem
        key = {item.id}
        id = {item.id}
        description = {item.text}
        complited = {item.complited}
        handleChange = {() => handleChange(item.id)}
        deleteTask = {() => deleteTask(item.id)}
      />
    );
  })

  return (
    <div className = "App" id='App'>
        <input value = {inputValue} onChange = {handleInput} type = "text" id='newTask'/>
        <button type = 'button' onClick = {addNewTask}>Add a new task</button>
        <div id = "tasks">
          {finalTasks}
        </div>
        
    </div>
  )
  

}

export default App;
